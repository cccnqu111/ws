import {ok, eq} from 'https://deno.land/x/tdd/mod.ts'
import * as rsa from "../mod.ts"

Deno.test('array', async function() {
    let key = rsa.genKeyPair()
    console.log('key=', key)
    let m = 123456789n
    let c = rsa.encrypt(key, m)
    let m2 = rsa.decrypt(key, c)
    console.log('m=', m)
    console.log('c=', c)
    console.log('m2=', m2)
    eq(m, m2)
})